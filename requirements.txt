aiobotocore @ file:///home/conda/feedstock_root/build_artifacts/aiobotocore_1641875803795/work
aiohttp @ file:///home/conda/feedstock_root/build_artifacts/aiohttp_1637087017014/work
aiohttp-retry @ file:///home/conda/feedstock_root/build_artifacts/aiohttp-retry_1632292602851/work
aioitertools @ file:///home/conda/feedstock_root/build_artifacts/aioitertools_1645842629199/work
aiosignal @ file:///home/conda/feedstock_root/build_artifacts/aiosignal_1636093929600/work
anyio==3.5.0
appdirs @ file:///home/conda/feedstock_root/build_artifacts/appdirs_1603108395799/work
asgiref==3.5.0
astroid @ file:///home/conda/feedstock_root/build_artifacts/astroid_1642536832077/work
asttokens @ file:///home/conda/feedstock_root/build_artifacts/asttokens_1618968359944/work
async-timeout @ file:///home/conda/feedstock_root/build_artifacts/async-timeout_1640026696943/work
asyncssh @ file:///home/conda/feedstock_root/build_artifacts/asyncssh_1638156214979/work
atpublic @ file:///home/conda/feedstock_root/build_artifacts/atpublic_1641909357516/work
attrs @ file:///home/conda/feedstock_root/build_artifacts/attrs_1640799537051/work
backcall @ file:///home/conda/feedstock_root/build_artifacts/backcall_1592338393461/work
backports.functools-lru-cache @ file:///home/conda/feedstock_root/build_artifacts/backports.functools_lru_cache_1618230623929/work
boto3 @ file:///home/conda/feedstock_root/build_artifacts/boto3_1639467547608/work
botocore @ file:///home/conda/feedstock_root/build_artifacts/botocore_1639449679425/work
brotlipy @ file:///home/conda/feedstock_root/build_artifacts/brotlipy_1636012198764/work
cached-property @ file:///home/conda/feedstock_root/build_artifacts/cached_property_1615209429212/work
certifi==2020.6.20
cffi @ file:///home/builder/ci_310/cffi_1642753365720/work
charset-normalizer @ file:///home/conda/feedstock_root/build_artifacts/charset-normalizer_1644853463426/work
click==8.1.0
colorama @ file:///home/conda/feedstock_root/build_artifacts/colorama_1602866480661/work
commonmark==0.9.1
configobj==5.0.6
cryptography @ file:///home/conda/feedstock_root/build_artifacts/cryptography_1639699266362/work
cycler @ file:///home/conda/feedstock_root/build_artifacts/cycler_1635519461629/work
dataclasses @ file:///home/conda/feedstock_root/build_artifacts/dataclasses_1628958434797/work
debugpy @ file:///home/conda/feedstock_root/build_artifacts/debugpy_1636043255643/work
decorator @ file:///home/conda/feedstock_root/build_artifacts/decorator_1641555617451/work
dictdiffer @ file:///home/conda/feedstock_root/build_artifacts/dictdiffer_1627027576828/work
diskcache @ file:///home/conda/feedstock_root/build_artifacts/diskcache_1640931331198/work
distro @ file:///home/conda/feedstock_root/build_artifacts/distro_1636872986284/work
dpath @ file:///home/conda/feedstock_root/build_artifacts/dpath_1643795643673/work
dulwich @ file:///home/conda/feedstock_root/build_artifacts/dulwich_1646515180422/work
dvc @ file:///home/conda/feedstock_root/build_artifacts/dvc_1646740462387/work
entrypoints @ file:///home/conda/feedstock_root/build_artifacts/entrypoints_1643888246732/work
executing @ file:///home/conda/feedstock_root/build_artifacts/executing_1646044401614/work
fastapi==0.75.0
flake8 @ file:///home/conda/feedstock_root/build_artifacts/flake8_1646781084538/work
flatten-dict @ file:///home/conda/feedstock_root/build_artifacts/flatten-dict_1629457542349/work
flufl.lock @ file:///home/conda/feedstock_root/build_artifacts/flufl.lock_1642030783107/work
fonttools @ file:///home/conda/feedstock_root/build_artifacts/fonttools_1643722419875/work
frozenlist @ file:///home/conda/feedstock_root/build_artifacts/frozenlist_1643222574384/work
fsspec @ file:///home/conda/feedstock_root/build_artifacts/fsspec_1645566723803/work
ftfy==5.5.1
funcy @ file:///home/conda/feedstock_root/build_artifacts/funcy_1640095670534/work
future @ file:///home/conda/feedstock_root/build_artifacts/future_1635819519073/work
gitdb @ file:///home/conda/feedstock_root/build_artifacts/gitdb_1635085722655/work
GitPython @ file:///home/conda/feedstock_root/build_artifacts/gitpython_1645531658201/work
grandalf==0.6
gssapi @ file:///home/conda/feedstock_root/build_artifacts/python-gssapi_1645400714655/work
h11==0.13.0
idna @ file:///home/conda/feedstock_root/build_artifacts/idna_1642433548627/work
importlib-metadata @ file:///home/conda/feedstock_root/build_artifacts/importlib-metadata_1646003251816/work
iniconfig @ file:///home/conda/feedstock_root/build_artifacts/iniconfig_1603384189793/work
ipykernel @ file:///home/conda/feedstock_root/build_artifacts/ipykernel_1644979762213/work/dist/ipykernel-6.9.1-py3-none-any.whl
ipython @ file:///home/conda/feedstock_root/build_artifacts/ipython_1646324684280/work
isort @ file:///home/conda/feedstock_root/build_artifacts/isort_1636447814597/work
jedi @ file:///home/conda/feedstock_root/build_artifacts/jedi_1637175074658/work
jmespath @ file:///home/conda/feedstock_root/build_artifacts/jmespath_1589369830981/work
joblib @ file:///home/conda/feedstock_root/build_artifacts/joblib_1633637554808/work
jupyter-client @ file:///home/conda/feedstock_root/build_artifacts/jupyter_client_1642858610849/work
jupyter-core @ file:///home/conda/feedstock_root/build_artifacts/jupyter_core_1645024262201/work
kiwisolver @ file:///home/conda/feedstock_root/build_artifacts/kiwisolver_1635836708241/work
lazy-object-proxy @ file:///home/conda/feedstock_root/build_artifacts/lazy-object-proxy_1639599010394/work
mailchecker @ file:///home/conda/feedstock_root/build_artifacts/mailchecker_1645643895665/work
matplotlib @ file:///home/conda/feedstock_root/build_artifacts/matplotlib-suite_1639358983313/work
matplotlib-inline @ file:///home/conda/feedstock_root/build_artifacts/matplotlib-inline_1631080358261/work
mccabe==0.6.1
multidict @ file:///home/conda/feedstock_root/build_artifacts/multidict_1643055359863/work
munkres==1.1.4
nanotime==0.5.2
nest-asyncio @ file:///home/conda/feedstock_root/build_artifacts/nest-asyncio_1638419302549/work
networkx @ file:///home/conda/feedstock_root/build_artifacts/networkx_1646497321764/work
numpy @ file:///home/conda/feedstock_root/build_artifacts/numpy_1646717167349/work
packaging @ file:///home/conda/feedstock_root/build_artifacts/packaging_1637239678211/work
pandas==1.4.1
parso @ file:///home/conda/feedstock_root/build_artifacts/parso_1638334955874/work
pathlib2 @ file:///home/conda/feedstock_root/build_artifacts/pathlib2_1644584165723/work
pathspec @ file:///home/conda/feedstock_root/build_artifacts/pathspec_1626613672358/work
patsy @ file:///home/conda/feedstock_root/build_artifacts/patsy_1632667180946/work
pexpect @ file:///home/conda/feedstock_root/build_artifacts/pexpect_1602535608087/work
phonenumbers @ file:///home/conda/feedstock_root/build_artifacts/phonenumbers_1645778698838/work
pickleshare @ file:///home/conda/feedstock_root/build_artifacts/pickleshare_1602536217715/work
Pillow==9.0.1
platformdirs @ file:///home/conda/feedstock_root/build_artifacts/platformdirs_1645298319244/work
pluggy @ file:///home/conda/feedstock_root/build_artifacts/pluggy_1635832822277/work
ply==3.11
prompt-toolkit @ file:///home/conda/feedstock_root/build_artifacts/prompt-toolkit_1644497866770/work
psutil @ file:///home/conda/feedstock_root/build_artifacts/psutil_1640887111623/work
ptyprocess @ file:///home/conda/feedstock_root/build_artifacts/ptyprocess_1609419310487/work/dist/ptyprocess-0.7.0-py2.py3-none-any.whl
pure-eval @ file:///home/conda/feedstock_root/build_artifacts/pure_eval_1642875951954/work
py @ file:///home/conda/feedstock_root/build_artifacts/py_1636301881863/work
pyasn1==0.4.8
pycodestyle @ file:///home/conda/feedstock_root/build_artifacts/pycodestyle_1633982426610/work
pycparser @ file:///home/conda/feedstock_root/build_artifacts/pycparser_1636257122734/work
pydantic==1.9.0
pydot @ file:///home/conda/feedstock_root/build_artifacts/pydot_1636047786876/work
pyflakes @ file:///home/conda/feedstock_root/build_artifacts/pyflakes_1633634815271/work
pygit2 @ file:///home/conda/feedstock_root/build_artifacts/pygit2_1643972987865/work
Pygments @ file:///home/conda/feedstock_root/build_artifacts/pygments_1641580240686/work
pygtrie @ file:///home/conda/feedstock_root/build_artifacts/pygtrie_1609712740518/work
pylint @ file:///home/conda/feedstock_root/build_artifacts/pylint_1638563695852/work
pyOpenSSL @ file:///home/conda/feedstock_root/build_artifacts/pyopenssl_1643496850550/work
pyparsing @ file:///home/conda/feedstock_root/build_artifacts/pyparsing_1635267989520/work
PySocks @ file:///home/conda/feedstock_root/build_artifacts/pysocks_1635862405051/work
pytest==7.0.1
python-benedict @ file:///home/conda/feedstock_root/build_artifacts/python-benedict_1633418865701/work
python-dateutil @ file:///home/conda/feedstock_root/build_artifacts/python-dateutil_1626286286081/work
python-fsutil @ file:///home/conda/feedstock_root/build_artifacts/python-fsutil_1643149438126/work
python-slugify @ file:///home/conda/feedstock_root/build_artifacts/python-slugify_1645990991155/work
pytz @ file:///home/conda/feedstock_root/build_artifacts/pytz_1633452062248/work
PyYAML @ file:///home/conda/feedstock_root/build_artifacts/pyyaml_1636139800922/work
pyzmq @ file:///home/conda/feedstock_root/build_artifacts/pyzmq_1635877336018/work
requests @ file:///home/conda/feedstock_root/build_artifacts/requests_1641580202195/work
rich @ file:///home/conda/feedstock_root/build_artifacts/rich_1644355353212/work
ruamel.yaml @ file:///home/conda/feedstock_root/build_artifacts/ruamel.yaml_1644759479433/work
ruamel.yaml.clib @ file:///home/conda/feedstock_root/build_artifacts/ruamel.yaml.clib_1636815627741/work
s3fs @ file:///home/conda/feedstock_root/build_artifacts/s3fs_1645586050824/work
s3transfer @ file:///home/conda/feedstock_root/build_artifacts/s3transfer_1645745825648/work
scikit-learn @ file:///home/conda/feedstock_root/build_artifacts/scikit-learn_1640464151742/work
scipy @ file:///home/conda/feedstock_root/build_artifacts/scipy_1644357243513/work
scmrepo @ file:///home/conda/feedstock_root/build_artifacts/scmrepo_1640164469434/work
seaborn @ file:///home/conda/feedstock_root/build_artifacts/seaborn-split_1629095986539/work
shortuuid @ file:///home/conda/feedstock_root/build_artifacts/shortuuid_1636643724603/work
shtab @ file:///home/conda/feedstock_root/build_artifacts/shtab_1641150030074/work
six @ file:///home/conda/feedstock_root/build_artifacts/six_1620240208055/work
smmap @ file:///home/conda/feedstock_root/build_artifacts/smmap_1611376390914/work
sniffio==1.2.0
stack-data @ file:///home/conda/feedstock_root/build_artifacts/stack_data_1644872665635/work
starlette==0.17.1
statsmodels @ file:///home/conda/feedstock_root/build_artifacts/statsmodels_1644535583699/work
tabulate @ file:///home/conda/feedstock_root/build_artifacts/tabulate_1614001031686/work
text-unidecode==1.3
threadpoolctl @ file:///home/conda/feedstock_root/build_artifacts/threadpoolctl_1643647933166/work
toml @ file:///home/conda/feedstock_root/build_artifacts/toml_1604308577558/work
tomli @ file:///home/conda/feedstock_root/build_artifacts/tomli_1644342247877/work
tornado @ file:///home/conda/feedstock_root/build_artifacts/tornado_1635819581833/work
tqdm @ file:///home/conda/feedstock_root/build_artifacts/tqdm_1646031859244/work
traitlets @ file:///home/conda/feedstock_root/build_artifacts/traitlets_1635260543454/work
typing_extensions @ file:///home/conda/feedstock_root/build_artifacts/typing_extensions_1644850595256/work
unicodedata2 @ file:///home/conda/feedstock_root/build_artifacts/unicodedata2_1640030999334/work
Unidecode @ file:///home/conda/feedstock_root/build_artifacts/unidecode_1645371071762/work
urllib3 @ file:///home/conda/feedstock_root/build_artifacts/urllib3_1641584929973/work
uvicorn==0.17.6
voluptuous @ file:///home/conda/feedstock_root/build_artifacts/voluptuous_1634129269635/work
wcwidth @ file:///home/conda/feedstock_root/build_artifacts/wcwidth_1600965781394/work
wrapt @ file:///home/conda/feedstock_root/build_artifacts/wrapt_1635836381710/work
xgboost==1.5.1
xlrd @ file:///home/conda/feedstock_root/build_artifacts/xlrd_1610224409810/work
xmltodict==0.12.0
yarl @ file:///home/conda/feedstock_root/build_artifacts/yarl_1636046807771/work
zc.lockfile==2.0
zipp @ file:///home/conda/feedstock_root/build_artifacts/zipp_1643828507773/work
