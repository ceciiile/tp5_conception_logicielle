from requete import Requete

def isVegan(requestAsJson):
    ingredients = requestAsJson["product"]["ingredients"]
    for i in ingredients:
        if ("vegan" not in i):
            i["vegan"] = True
        if not i["vegan"]:
            return False
    return True


api = Requete()
#compote = api.infos(3560070967209)
#print(compote["product"]["ingredients"])
#print(len(compote["product"]["ingredients"]))
#print(isVegan(compote))

test = api.infos(3560070967209)
#print(test["product"]["ingredients"])
print(isVegan(test))